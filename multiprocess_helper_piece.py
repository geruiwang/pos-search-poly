import numpy as np
from numpy.polynomial.polynomial import polyval

from simulate_pos_polynomial import *
from multiprocessing import Process, Queue, freeze_support
import json
import os

class MultiprocessHelper():
    queue = None
    def __init__(self, A=1, B=2, NumPros = 11, T=1000, NumRows = 10):
        self.A=A
        self.B=B
        self.NumPros = NumPros
        self.T=T
        self.NumRows = NumRows
    def put_queue(self):
        for c in piecewise_generator(5):
            self.queue.put(json.dumps(c))
        for i in range(2*self.NumPros):
            self.queue.put('[0]')
    def get_queue(self,output):
        if type(output) is not str:
            print('Output error')
            return
        
        A=self.A
        B=self.B
        T=self.T
        cof_list = []
        res_list = []
        coefficients=[]
        c = self.queue.get()
        while c!='[0]':
            c_ = json.loads(c)
            factors = np.ones(T)
            for i in range(5):
                factors[i*200:(i+1)*200] = c_[i]*factors[i*200:(i+1)*200]
            if (factors>=0).all():
                factors_sum = np.sum(factors)#normalize
                factors = factors*T/factors_sum
                cof_list.append(c)
                coefficients.append(factors)
                if len(coefficients)==self.NumRows:
                    res = simulate_pos(np.array(coefficients), A,B)
                    res_list.extend(res.tolist())
                    coefficients=[]
            c = self.queue.get()
        if len(coefficients)>0:
            res = simulate_pos(np.array(coefficients), A,B)
            res_list.extend(res.tolist())
        output = open(output,'w')
        json.dump([cof_list,res_list],output)
        output.close()
    def run_all(self,output):
        # start one process, put sth into queue
        # start many pros, read queue
        self.queue = Queue()
        p0 = Process(target=self.put_queue,args=())
        p0.start()
        #self.put_queue()
        print('--start-----------')
        #self.get_queue('tmp.txt')
        pros = []
        if type(output) is str:
            for i in range(1,self.NumPros+1):
                p = Process(target=self.get_queue,args=(output+str(i).zfill(2),))
                pros.append(p)
                p.start()
                print('--sub-{}----------'.format(i))
        p0.join()
        for p in pros:
            p.join()
        self.queue.close()
        #command_cat='cat'
        #for i in range(1,self.NumPros+1):
        #    command_cat += ' '+output+str(i).zfill(2)
        #command_cat += ' > '+output
        #os.system(command_cat)
        #for i in range(1,self.NumPros+1):
        #    command_del = 'rm '+output+str(i).zfill(2)
        #    os.system(command_del)
    def read_results(self,output):
        A=self.A
        B=self.B
        T=self.T
        coefficients = []
        results = []
        for i in range(1,self.NumPros+1):
            filename = output+str(i).zfill(2)
            with open(filename) as fin:
                for line in fin:
                    tmp=json.loads(line)
                    coefficient = tmp[0]
                    result = tmp[1]
                    coefficients.extend(coefficient)
                    results.extend(result)
        assert len(results)==len(coefficients)
        trial = len(results[0])
        
        l_emd = []
        for i in range(len(results)):
            ans = np.sum(np.abs(np.array(results[i])-A/(A+B)))/trial
            l_emd.append((ans,coefficients[i]))
        l_se = []
        for i in range(len(results)):
            ans = np.sum((np.array(results[i])-A/(A+B))*(np.array(results[i])-A/(A+B)))/trial
            l_se.append((ans,coefficients[i]))
        l_absfair={0.1:[],0.01:[],0.001:[]}
        for eps in l_absfair:
            for i in range(len(results)):
                ans = np.sum(np.abs(np.array(results[i])-A/(A+B))>eps)/trial
                l_absfair[eps].append((ans,coefficients[i]))
        l_relfair={0.1:[],0.01:[],0.001:[]}
        for eps in l_absfair:
            for i in range(len(results)):
                ans = np.sum(np.abs(np.array(results[i])-A/(A+B))>eps*A/(A+B))/trial
                l_relfair[eps].append((ans,coefficients[i]))
        return {"emd":l_emd, "se": l_se, "absfair": l_absfair, "relfair": l_relfair}
    #def plotpoly(c):
    #    x=np.linspace(0,1)
    #    y=polyval(x,c)
    #    #y=y/np.sum(y)
    #    plt.plot(x,y,'-')
    #    plt.title("poly coef {}".format(c))
    #    plt.show()
    #self.run_all("newpoly{},{}.txt".format(self.A,self.B))

if __name__ == "__main__":
    #freeze_support()
    helper = MultiprocessHelper()
