import json
import numpy as np
from numpy.polynomial.polynomial import polyval

def simulate_pos(factors, A=2, B=4, threshold = 1, trial = 10000, epoch = 1 , sec_para=1000000,  plot_every = None):
    '''
    A, B: intial stakes
    threshold: selfish mining threshold
    T: iteration of simulation
    trial: number of trials
    epoch: recompute stakes every epoch
    sec_para: security parameter to reject old history change (set to very large by default)
    coefficients: for polynomial function of reward
    plot_every: how many iterations to make a plot
    '''
    T = factors.shape[1]
    num = factors.shape[0]
    factors = np.tile(factors, (trial,1))
    old_trial = trial
    trial = trial * num
    # static stake prob.
    a=A*np.ones(trial) # init A's blocks in the chain
    b=B*np.ones(trial) # init B's blocks in the chain
    randomstring = np.random.rand(T,trial)
    side = np.zeros(trial) # init A's side chain
    side_rwd = np.zeros(trial)
    honest = np.zeros(trial) # init B's honest chain
    honest_rwd = np.zeros(trial)
    results_list = []
    results = a/(a+b+honest_rwd) # current stake ratio
    recompute_each_epoch = np.copy(results) # recompute ratio each epoch
    for i in range(T):
        tmp = (randomstring[i] < recompute_each_epoch).astype(np.double) # newly generated leader
        #tmp = (randomstring[i] < A/(A+B)).astype(np.double) # newly generated leader
        side = side + tmp
        side_rwd = side_rwd + tmp*factors[:,i]
        honest = honest + (1-tmp)
        honest_rwd = honest_rwd + (1-tmp)*factors[:,i]
        #selfish = (a/(a+b+honest) > threshold).astype(np.double)
        selfish = (recompute_each_epoch > threshold).astype(np.double)

        longer = (side > honest).astype(np.double) # NOT do selfish mining part
        a = a+ side_rwd*(1-selfish)*(longer)
        b = b+ honest_rwd*(1-selfish)*(1-longer)
        side = side*selfish
        side_rwd = side_rwd*selfish
        honest = honest*selfish
        honest_rwd = honest_rwd*selfish

        #override = ( (side > honest)*(honest>0)*(honest<sec_para) ).astype(np.double) # selfish mining part
        override = ( (side == honest+1)*(honest>=1)*(honest<sec_para) ).astype(np.double) # selfish mining part
        adopt = ((side < honest)+(honest>=sec_para) ).astype(np.double)
        assert (override*selfish == override).all()
        assert (adopt*selfish == adopt).all()
        a =  a + side_rwd*override
        side = side * (1-override)
        side_rwd = side_rwd * (1-override)
        honest = honest * (1-override)
        honest_rwd = honest_rwd * (1-override)

        b = b + honest_rwd * adopt
        honest = honest * (1-adopt)
        honest_rwd = honest_rwd * (1-adopt)
        side = side * (1-adopt)
        side_rwd = side_rwd * (1-adopt)
        results = a/(a+b+honest_rwd)
        if plot_every is not None and (i+1)%plot_every==0:
            #results_list.append((i+1,a/(a+b)))
            results_list.append((i+1,np.copy(results)))
        if sec_para<T and (i+1)%epoch == (-2*sec_para)%epoch: # simulate Ouroboros
            stake_before2k = np.copy(results)
        if (i+1)%epoch == 0:
            if sec_para<T: # simulate Ouroboros
                recompute_each_epoch = np.copy(stake_before2k)
            else:
                recompute_each_epoch = np.copy(results)
    if plot_every is None:
        results_list.append((T,np.copy(results)))
    results = results_list[-1][-1]
    reshape_results = np.zeros((num,old_trial))
    for i in range(num):
        reshape_results[i] = results[i::num]
    return reshape_results
def point2poly(c3, inflect, extreme_away, c0):# since it will be normalized, c3 can pick 1 or -1, and change c0. Also extreme_away only matters with abs value. Notice it may reach negative, so keep alert!
    assert c3!=0
    c2=inflect*(-3*c3)
    delta = np.real(extreme_away*extreme_away*(36*c3*c3))
    c1=(4*c2*c2-delta)/(12*c3)
    return [c0,c1,c2,c3]
def poly_generator(step2=0.1, step1=0.1, step0=1.0):
    for c3 in [-1,1]:
        for c2 in np.arange(-1,2.1,step2):
            for c1 in np.arange(0,5.1,step1):
                for c0 in [0.0]:#np.arange(0,5.5,step0):
                    c=point2poly(c3,c2,c1,c0)
                    yield c
def piecewise_generator(piece):
    if piece==5:
        for c4 in [1,2,4,8,16,32]:
            for c3 in [1,2,4,8,16,32]:
                for c2 in [1,2,4,8,16,32]:
                    for c1 in [1,2,4,8,16,32]:
                        c=[1,c1,c2*c1,c3*c2*c1,c4*c3*c2*c1]
                        yield c
